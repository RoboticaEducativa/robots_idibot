/*
* Código para Robot Futbolista Educativo V1
* By: Jhon Jairo Bravo Castro
* Email: yjairobravo@gmail.com
* 
*/
#include <Servo.h>

Servo pinza;  // create servo object to control a servo

int initPWM = 80;
void setup() {
  // put your setup code here, to run once:
  pinza.attach(9);  // attaches the servo on pin 9 to the servo object
pinMode(2,OUTPUT);
pinMode(3,OUTPUT);

pinMode(4,OUTPUT);
pinMode(5,OUTPUT); //PWM
//analogWrite(11,100);
Serial.begin(9600);
openPinza();
delay(2000);
stop();
}

void loop() {

if(Serial.available()){
  int data = Serial.read();


 //Serial.print(data);
 //Serial.println();

 switch(data){
  case 97:
  stop();
   atras();
    break;
  case 98:
  stop();
   adelante();
    
    break;
  case 99:
  stop();
     izquierda();
    break;
  case 100:
  stop();
  derecha();
   
    
    break;
  case 101:
  stop();
    break;
  case 102:
    closePinza();
    break;
  case 103:
    openPinza();
    
    break;          
 }

}
/*
if(analogRead(A0) > 400){
  atras();
  delay(200);
  stop();
}*/
 delay(20);

}

void closePinza(){
  pinza.write(map(100, 0, 1023, 90, 220));
}
void openPinza(){
  pinza.write(map(1023, 0, 1023, 50, 220));
}
void stop(){

   digitalWrite(2,LOW);
   digitalWrite(3,LOW);
   digitalWrite(4,LOW);
   digitalWrite(5,LOW);
}


void derecha(){
  initPWM = 20;
  digitalWrite(2,HIGH);
  digitalWrite(4,LOW);
 
for(int i = 0; i < 10; i++){
      delay(5);
      analogWrite(3,initPWM);
       analogWrite(5,initPWM); 
      initPWM += 3;
    }
   
   
}


void izquierda(){
  initPWM = 20;
  digitalWrite(2,LOW); 
  digitalWrite(4,HIGH);

  for(int i = 0; i < 10; i++){
      delay(5);
       analogWrite(3,initPWM);
      analogWrite(5,initPWM);
      initPWM += 3;
    }
   
}
void adelante(){
   digitalWrite(2,LOW); //ADELANTE
   digitalWrite(3,HIGH); 
   digitalWrite(5,HIGH);
   digitalWrite(4,LOW);
}

void atras(){
   digitalWrite(2,HIGH); //ATRAS
   digitalWrite(3,LOW); 
   digitalWrite(5,LOW);
   digitalWrite(4,HIGH); 
}
