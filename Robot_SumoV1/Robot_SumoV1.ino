/*
* Código para robot Minisumo Educativo V1
* By: Jhon Jairo Bravo Castro
* Email: yjairobravo@gmail.com
* 
*/

#define MOTOR_1A 2
#define MOTOR_1B 3

#define MOTOR_2A 4
#define MOTOR_2B 5

#define BUTTON_START 13

#define SENSOR_PISO_DERECHA 6
#define SENSOR_PISO_IZQUIERDA 7

int statusStart = 1;
void setup() {
  
pinMode(9,INPUT);
pinMode(10,INPUT);

pinMode(BUTTON_START,INPUT);

pinMode(MOTOR_1A,OUTPUT);
pinMode(MOTOR_1B,OUTPUT);
pinMode(MOTOR_2A,OUTPUT);
pinMode(MOTOR_2B,OUTPUT);
stopSumo();

while(statusStart){
  if(digitalRead(BUTTON_START)){
    statusStart = 0;
  }
  
}
delay(5000);
}

void loop() {
 adelante();
 
 if(digitalRead(SENSOR_PISO_DERECHA) || digitalRead(SENSOR_PISO_IZQUIERDA) ){
  stopSumo();
  unsigned long millisReadInit = millis() + 1000 ;
  while(millisReadInit > millis()){
   atras();
    giroConLecturaIzquierda();
    giroConLecturaDerecha();
  } 
 int valueRan =  random(1, 2);
 if(valueRan == 1){
  derecha();
    delay(200);
 }else{
  izquierda();
    delay(200);
 }
  adelante();
 }

giroConLecturaIzquierda();
giroConLecturaDerecha();

}

void giroConLecturaIzquierda(){
   if(analogRead(A0) > 400){
  stopSumo();
  while(analogRead(A0) > 400){
    derecha();
    delay(200);
  }
  stopSumo();
 }
}

void giroConLecturaDerecha(){
   if(analogRead(A1) > 400){
  stopSumo();
  while(analogRead(A1) > 400){
     izquierda();
     delay(200);
  }
 
  stopSumo();
 }
}
void adelante(){
  digitalWrite(MOTOR_1A,LOW);
  digitalWrite(MOTOR_1B,HIGH);
  digitalWrite(MOTOR_2A,LOW);
  digitalWrite(MOTOR_2B,HIGH);
}
void stopSumo(){
  digitalWrite(MOTOR_1A,LOW);
  digitalWrite(MOTOR_1B,LOW);
  digitalWrite(MOTOR_2A,LOW);
  digitalWrite(MOTOR_2B,LOW);
}

void atras(){
  digitalWrite(MOTOR_1A,HIGH);
  digitalWrite(MOTOR_1B,LOW);
  digitalWrite(MOTOR_2A,HIGH);
  digitalWrite(MOTOR_2B,LOW);
}

void derecha(){
  digitalWrite(MOTOR_1A,HIGH);
  digitalWrite(MOTOR_1B,LOW);
  
  digitalWrite(MOTOR_2A,LOW);
  digitalWrite(MOTOR_2B,HIGH);
}

void izquierda(){
   digitalWrite(MOTOR_1A,LOW);
  digitalWrite(MOTOR_1B,HIGH);
  
  digitalWrite(MOTOR_2A,HIGH);
  digitalWrite(MOTOR_2B,LOW);
}
